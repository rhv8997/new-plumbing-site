const { slugify } = require("./src/util/utilityFunctions")
const path = require("path")
const authors = require("./src/util/authors")
const _ = require("lodash")

exports.onCreateNode = ({ node, actions }) => {
  const { createNodeField } = actions
  if (node.internal.type === "MarkdownRemark") {
    const slugFromtitle = slugify(node.frontmatter.title)
    createNodeField({
      node,
      name: "slug",
      value: slugFromtitle,
    })
  }
}

exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions

  const templates = {
    aboutUs: path.resolve("src/templates/about-us.js"),
    contactUs: path.resolve("src/templates/contact-us.js"),
    plumbingServices: path.resolve("src/templates/plumbing-services.js"),
    heatingServices: path.resolve("src/templates/heating-services.js"),
  }

  createPage({
    path: "/about-us",
    component: templates.aboutUs,
  })
  createPage({
    path: "/contact-us",
    component: templates.contactUs,
  })
  createPage({
    path: "/plumbing-services",
    component: templates.plumbingServices,
  })
  createPage({
    path: "/heating-services",
    component: templates.heatingServices,
  })
}
