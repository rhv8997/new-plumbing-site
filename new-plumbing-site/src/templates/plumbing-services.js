import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { Card, CardTitle, CardBody, CardText } from "reactstrap"
import { Link } from "gatsby"
import tap from "../images/tap.jpg"
import bathroom from "../images/bathroom.jpg"

const PlumbingServices = () => {
  return (
    <Layout>
      <SEO title="About Us" />
      <Card className="plumbing-cards">
        <CardBody>
          <CardTitle>
            <h1 className="section-header">
              {" "}
              Are You Looking For A Plumber In Swindon?
            </h1>
            <h1 className="tagline-header">
              Call Us For An Honest & Friendly Plumbing Service That You Can
              Rely On
            </h1>
          </CardTitle>
          <CardText>
            GJV Plumbing & Heating is a client focused, locally based
            professional plumber in Swindon. We cover all types of domestic
            plumbing for all of your home plumbing servicing, repair or
            installation. We give you full peace of mind being a Gas Safe
            registered plumber. We cover many areas for plumbing services,
            including Cricklade, Lambourn, Marlborough, Shrivenham, Devizes,
            Purton and Wootten Bassett.
            <img src={tap} width="100%" alt="Picture of a tap" />{" "}
          </CardText>
          <CardTitle>
            <h1 className="section-header">Our Plumbing Work</h1>
            <h1 className="tagline-header">
              Friendly, Reliable & Professional
            </h1>
          </CardTitle>
          <CardText>
            If you are looking for a reliable and experienced plumber? From
            installing a bathroom suite to mending a leaky tap GJV Plumbing and
            Heating are here to carry out the work in you home with as little
            disruption as possible.
            <li>General Plumbing</li>
            <li>Water Leaks</li>
            <li>Taps Repairs and Replacements</li>
            <li>Toilets</li>
            <li>Bathroom Fittings</li>
            <li>Heating Problems</li>
            <li>Power Flush</li>
            <li>Water Tanks</li>
            <li>Burst Pipes</li>
            <li>Boiler Repairs and Replacements</li>
            <li>Showers</li>
            <img src={bathroom} width="100%" alt="Picture of a bathroom" />{" "}
          </CardText>
          <CardTitle>
            <h1 className="section-header">Bathroom installations Swindon</h1>
            <h1 className="tagline-header">
              At GJV Plumbing And Heating We Offer A Complete Bathroom Service.
            </h1>
          </CardTitle>
          <CardText>
            <li>New Bathrooms Designed, Sourced And Fitted</li>
            <li>Individual Component Repair (Bath, Toilet, Basin, Taps Etc)</li>
            <li>All Shower Installations</li>
            <li>Towel Rails</li>
            <li>Shower Pumps</li>
          </CardText>
        </CardBody>
      </Card>
    </Layout>
  )
}

export default PlumbingServices
