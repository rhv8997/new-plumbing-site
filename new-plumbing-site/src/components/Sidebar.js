import React from "react"
import { Row, Col, Card, CardTitle, CardBody, CardText } from "reactstrap"
import { graphql, StaticQuery } from "gatsby"
import gasSafe from "../images/gasSafe.jpg"
import constructionLine from "../images/constructionLine.jpg"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faTty,
  faMobile,
  faEnvelope,
  faPhoneSquareAlt,
} from "@fortawesome/free-solid-svg-icons"

import { faFacebookSquare } from "@fortawesome/free-brands-svg-icons"

const Sidebar = ({ author, authorFluid }) => (
  <div>
    <Card className="sidebar-cards">
      <CardBody>
        <CardTitle className="text-center text-uppercase mb-3 sidebar-card-title">
          Swindon Plumber
        </CardTitle>
        <CardText>
          GJV plumbing and heating are based in Swindon but cover a 50 mile
          radius including:
          <li>Swindon</li>
          <li>Newbury</li>
          <li>Highworth</li>
          <li>Chippenham</li>
          <li>Marlbourgh</li>
          <li>Wroughton</li>
          <li>Carterton</li>
          <li>Witney</li>
          <li>Malmsbury</li>
          <li>Wanbrough</li>
          Find out more, give us a call on 01793 523335
        </CardText>
      </CardBody>
    </Card>
    <Card className="sidebar-cards middle-sidebar-card">
      <CardBody>
        <CardTitle className="text-center text-uppercase mb-3 middle-sidebar-card-title">
          Get In Touch
        </CardTitle>
        <CardText>
          <ul>
            <li>
              {" "}
              <Row>
                <Col md="1" className="float-right">
                  <FontAwesomeIcon icon={faTty} size="lg" />
                </Col>
                <Col>01793 523335</Col>
              </Row>
            </li>
            <li>
              {" "}
              <Row>
                <Col md="1" className="float-right">
                  <FontAwesomeIcon icon={faPhoneSquareAlt} size="lg" />
                </Col>

                <Col>07734 176129</Col>
              </Row>
            </li>
            <li>
              {" "}
              <Row>
                <Col md="1" className="contact-icons">
                  <FontAwesomeIcon icon={faEnvelope} size="lg" />
                </Col>

                <Col>info@gjvplumbingandheating.co.uk</Col>
              </Row>
            </li>
            <li>
              {" "}
              <Row>
                <Col md="1" className="contact-icons">
                  <FontAwesomeIcon icon={faFacebookSquare} size="lg" />
                </Col>

                <Col>Find us on Facebook</Col>
              </Row>
            </li>
          </ul>
        </CardText>
      </CardBody>
    </Card>

    <Card className="sidebar-cards">
      <CardBody>
        <CardTitle className="text-center text-uppercase mb-3 sidebar-card-title">
          GJV Plumbing and Heating are...
        </CardTitle>
        <CardText>
          <li>Fully Insured</li>
          <li>Gas Safe Registered</li>
        </CardText>
        <img src={gasSafe} width="100%" alt="Gase Safe Logo" />{" "}
        <img
          src={constructionLine}
          width="100%"
          alt="Vaillant, Construction Line, Kamco logos"
        />{" "}
      </CardBody>
    </Card>
  </div>
)

export default Sidebar
