import React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faFacebook,
  faTwitter,
  faInstagram,
  faLinkedin,
} from "@fortawesome/free-brands-svg-icons"

const Footer = () => (
  <div className="site-footer">
    <p>
      Home | Plumbing Service | Heating Services | About Us | Contact Us | Site
      Map | Find us on Call My Local ©Property GJV Plumbing & Heating. 2011. 57
      Groundwell Road, Swindon, Wiltshire SN1 2LY| Web design & SEO Bristol. |
      Find us on: Thompson Local | Free Index Find us on Bizwiki | Leave a
      review on Google
    </p>
  </div>
)

export default Footer
