/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

import Header from "./header"
import Footer from "./Footer"
import { Row, Col } from "reactstrap"
import Sidebar from "./Sidebar"

import "../styles/index.scss"

const Layout = ({ children, pageTitle, authorImageFluid, postAuthor }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <>
      <script
        src="https://kit.fontawesome.com/9f5076fc32.js"
        crossOrigin="anonymous"
      ></script>

      <Header siteTitle={data.site.siteMetadata.title} />
      <div className="container" id="content">
        <h2 className="layout-header">{pageTitle}</h2>

        <Row>
          <Col md="8">{children}</Col>
          <Col md="4">
            <Sidebar author={postAuthor} authorFluid={authorImageFluid} />
          </Col>
        </Row>
      </div>
      <Footer />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
