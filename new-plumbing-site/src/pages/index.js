import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { Card, CardTitle, CardBody, CardText } from "reactstrap"
import { Link } from "gatsby"

const IndexPage = () => {
  return (
    <Layout pageTitle=" 	&nbsp;">
      <SEO title="Home" />
      <Card className="plumbing-cards">
        <CardBody>
          <CardTitle>
            <h1 className="section-header">
              {" "}
              Plumber In Swindon - Plumbing And Heating Experts
            </h1>
            <h1 className="tagline-header">Call Us Today T:01793 523 335</h1>
          </CardTitle>
          Are you looking for plumbing & heating services in Swindon? GJV
          Plumbing and Heating is a friendly and professional Plumber in
          Swindon, covering many surrounding areas such as Cricklade, Lambourn,
          Marlborough, Shrivenham, Devizes, Purton and Wootten Bassett. We cover
          all aspects of plumbing and heating & install everything from taps,
          kitchen sinks to pipe-work, bathrooms, baths, showers, toilets,
          drainage, power flushing, utilities to full central heating systems &
          gas boilers for your home. As we are Gas Safe registered (No.132329)
          you have the guarantee that you will receive a high standard of work
          and a reliable service.
          <br />
          <Link className="btn btn-outline-primary float-right read-more">
            Read More
          </Link>
        </CardBody>
      </Card>
      <Card className="plumbing-cards">
        <CardBody>
          <CardTitle>
            <h1 className="section-header">Our Plumbing Work</h1>
            <h1 className="tagline-header">
              Friendly, Reliable & Professional
            </h1>
          </CardTitle>
          If you are looking for a reliable and experienced plumber? From
          installing a bathroom suite to mending a leaky tap GJV Plumbing and
          Heating are here to carry out the work in you home with as little
          disruption as possible.
          <li>General Plumbing</li>
          <li>Water Leaks</li>
          <li>Taps Repairs and Replacements</li>
          <li>Toilets</li>
          <li>Bathroom Fittings</li>
          <li>Heating Problems</li>
          <li>Power Flush</li>
          <li>Water Tanks</li>
          <li>Burst Pipes</li>
          <li>Boiler Repairs and Replacements</li>
          <li>Showers</li>
          <Link className="btn btn-outline-primary float-right read-more">
            Read More
          </Link>
        </CardBody>
      </Card>
      <Card className="plumbing-cards">
        <CardBody>
          <CardTitle>
            <h1 className="section-header">Our Heating Services </h1>
            <h1 className="tagline-header">
              We Specialise In Household Heating Services
            </h1>
          </CardTitle>
          GJV plumbing and heating specialise in boiler and central heating
          installations. Our installers are gas safe registered and LPG gas
          registered.
          <li>Entire Heating Systems </li>
          <li>Hot Water Cylinders </li>
          <li>Boiler Installations And Replacement </li>
          <li>Radiators And Heating Controls </li>
          <li>Full Range Of Boilers Available </li>
          <li>Unvented Cylinders</li>
          <Link className="btn btn-outline-primary float-right read-more">
            Read More
          </Link>
        </CardBody>
      </Card>
    </Layout>
  )
}

export default IndexPage
